package util_test

import (
	"testing"

	"github.com/onsi/ginkgo"
	"github.com/onsi/gomega"
)

func setup() interface{} {
	gomega.RegisterFailHandler(ginkgo.Fail)
	return nil
}

var _ = setup()

func TestInit(t *testing.T) {
	ginkgo.RunSpecs(t, "gitlab.com/nestoroprysk/pipeline-greenhouse/util")
}
