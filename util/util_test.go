package util_test

import (
	"time"

	"gitlab.com/nestoroprysk/pipeline-greenhouse/util"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = It("ID", func() {
	time.Sleep(1 * time.Minute)
	Expect(util.ID("a")).To(Equal("a"))
})
