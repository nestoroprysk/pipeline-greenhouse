package util2_test

import (
	"time"

	"gitlab.com/nestoroprysk/pipeline-greenhouse/util2"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = It("ID", func() {
	time.Sleep(1 * time.Minute)
	Expect(util2.ID(1)).To(Equal(1))
})
