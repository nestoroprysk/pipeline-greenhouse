pipeline-greenhouse is a greenhouse for experimenting with the Gitlab CI.

The most important two places are th Gitlab CI config file (.gitlab-ci.yml) and
a pre commit hooks file (.pre-commit).

Note that running `ln -sf $(pwd)/.pre-commit  .git/hooks/pre-commit` is necessary to configure client side hooks.
