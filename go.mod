module gitlab.com/nestoroprysk/pipeline-greenhouse

go 1.13

require (
	github.com/onsi/ginkgo v1.15.2
	github.com/onsi/gomega v1.11.0
)
